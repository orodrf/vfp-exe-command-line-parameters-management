# VFP SOME SPECIAL CLASS & FUNCTIONS, ALGUNAS CLASES Y FUNCIONES ESPECIALES

..............................................................

#OPARAM.PRG & TEST.PRG:
Command line parameters management in a EXE build by VFP8 or later (FOXPRO).

Gestor de linea de comandos de un proyecto ejecutable de VFP8 o posterior.
Basicamente gestiona la linea de parametros pasados al exe o prg principal, 
lo pone en un objeto y permite comprobar si existe una clave y obtener su valor 

supongamos: projectvfp.exe --temp C:\TEMP --usr admin --pass 123

El objeto hace la separacion clave valor y lo mete en una matriz interna que
permite comprobar si existe una clave y obtener su valor.


para instalarlo copiar la definicion de la clase oparam.prg en el proyecto y 
cargarlo en el prg principal medidante SET PROCEDURE TO oparam.prg y agregar
la linea LPARAMETERS lcPARAMETERS en la primer linea y meter las lineas de 
creacion de instancia del objeto luego del SET PROCEDURE TO oparam.prg

loPAR=CREATEOBJECT( 'OPARAM', lcPARMETER, '--', ' ' )


En el archivo test.prg estan las pruebas y/o modos de uso que pueden probar 
en el IDE del VFP.


..............................................................

#ANSIANDUTF8.PRG

Convert from ANSI/ASCII string code to UTF8 string and reverse. Test in VFP8.

Convierte una cadena codificada en ANSI/ASCII a UTF8 y viceversa. Probado en 
VFP8

..............................................................

