*//////////////////////////////////////////////////////////////////////////////
*//		CLASE PARA PROCESAR CADENA DE PARAMETROS DE EXE O PRG PRINCIPAL
*//////////////////////////////////////////////////////////////////////////////
DEFINE CLASS OPARAM AS CUSTOM
	* los parametros se pasan asi		&&	--USER admin --PASS x --HOST 127.0.0.1
	PROTECTED laKEYVAL[1],lnKEYCAN,lcEXEPAR,lcSEPKEY,lcSEPVAL
	lnKEYCAN	=0						&&	cantidad de pares key-value
	lcEXEPAR	=""						&&	cadena de parametros
	lcSEPKEY	="--"					&&	separador de pares key-valor --HOST 127.0.0.1
	lcSEPVAL	=" "					&&	separador de key-valor individual
	*////////////////
	PROCEDURE INIT
	*////////////////
		*//
		*//	TODO:	Procesa los parametros pasados al prg y los parsea
		*//
		LPARAMETERS lcEXEPAR,lcSEPKEY,lcSEPVAL
		LOCAL lcSEPKEY,lcSEPVAL,lnLENARR,lnCANLIN,lnGIR,lcLINEA,lcKEYNAM,lcKEYVAL
		
		* sin parametros levanta el objeto igual
		IF TYPE('lcEXEPAR')=='L' OR EMPTY(lcEXEPAR)
			RETURN .T.
		ENDIF
		* separador de tag-valor
		IF TYPE('lcSEPKEY')=='C' AND !EMPTY(lcSEPKEY)
			THIS.lcSEPKEY=lcSEPKEY
		ENDIF
		* separador de valor
		IF TYPE('lcSEPVAL')=='C' AND !EMPTY(lcSEPVAL)
			THIS.lcSEPVAL=lcSEPVAL
		ENDIF		
		
		* copiamos parametros a propiedad
		THIS.lcEXEPAR	=lcEXEPAR
		lnLENARR		=ALINES( laKEYVAL, STRTRAN( lcEXEPAR, THIS.lcSEPKEY, CHR(13) ) )
		lnCANLIN		=0
		FOR lnGIR=2 TO lnLENARR STEP 1	&&omitimos el nombre del ejecutable
			
			lcLINEA	=ALLTRIM( laKEYVAL[lnGIR] )										&&KEYVALUE:	[--USER admin]
			lcKEYNAM=ALLTRIM( SUBSTR( lcLINEA, 1, AT(THIS.lcSEPVAL,lcLINEA)-1 ) )
			lcKEYVAL=ALLTRIM( SUBSTR( lcLINEA, AT(THIS.lcSEPVAL,lcLINEA)+1 ) )
			lnCANLIN=lnCANLIN+1 
			
			DIMENSION THIS.laKEYVAL[ lnCANLIN, 2 ]
			THIS.laKEYVAL[ lnCANLIN, 1 ]=lcKEYNAM
			THIS.laKEYVAL[ lnCANLIN, 2 ]=lcKEYVAL
			
		ENDFOR
		THIS.lnKEYCAN=lnCANLIN
		IF lnCANLIN==0
			MESSAGEBOX( [OPARAM::INIT(), AL PARSEAR], 16, [ERROR])
			RETURN .F.
		ENDIF
		
		RETURN .T.
	ENDPROC
	*/////////////////////////////////////////
	FUNCTION CHKISKEY( lcFNDKEY ) AS BOOLEAN
	*/////////////////////////////////////////
		*//
		*//	TODO:	AVERIGUA SI UNA CLAVE EXISTE Y DEVUELVE TRUE
		*//
		RETURN IIF( ASCAN(THIS.laKEYVAL,lcFNDKEY,1,0,0,1)>0, .T., .F. )
	ENDFUNC
	*/////////////////////////////////////////////
	FUNCTION GETVALKEY( lcKEYVAL ) AS CHARACTER
	*/////////////////////////////////////////////
		*//
		*//	TODO:	DEVUELVE EL VALOR DE UNA CLAVE SI EXISTE
		*//
		IF !THIS.CHKISKEY( lcKEYVAL )
			RETURN ''
		ENDIF
		RETURN THIS.laKEYVAL[ ASUBSCRIPT(THIS.laKEYVAL,ASCAN(THIS.laKEYVAL,lcKEYVAL,1,0,0,1),1), 2 ]
	ENDFUNC
	*/////////////////////////////////
	FUNCTION GETLENKEYS() AS INTEGER
	*/////////////////////////////////
		*//
		*//	TODO:	DEVUELVE LA LONGITUD DE MATRIZ DE PARAMETROS
		*//
		RETURN THIS.lnKEYCAN
	ENDFUNC
	*/////////////////////////////////////
	FUNCTION GETEXEPARAM() AS CHARACTER
	*/////////////////////////////////////
		*//
		*//	TODO:	DEVUELVE LA CADENA DE PARAMETRO DEL EXE O PRG
		*//
		RETURN THIS.lcEXEPAR
	ENDFUNC
ENDDEFINE
